<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\NotaController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::redirect('/', '/notas');

//    return view('welcome');
//Route::post('/notas/results', [NotaController::class, 'search'])->name('notas.results');
Route::get('/notas/search', [NotaController::class, 'search'])->name('notas.search');
Route::post('/notas/results', [NotaController::class, 'results'])->name('notas.results');
Route::resource('/notas','App\Http\Controllers\NotaController');
