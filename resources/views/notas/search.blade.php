@extends('layouts.master')

@section('title') Anotações old black web @endsection
@section('content')
<br><a href="http://www.brdsoft.com.br"><img src="/logo-azul-com-texto.png"style="width:120px;height:60px;"></a><br>
  <form method="post" action="{{ route ('notas.results') }}">
        @csrf
        @method('POST')
        <div class="card card-default">
            <div class="card-header">
                Pesquisar anotações
            </div>
            <div class="card-body">
                <div class="form-group">
                    <label for="pchave">Palavra chave</label>
                    <input type="input" class="form-contro"l  name="pchave">
                </div>
            </div>
            <div class="card-footer">
                <button class="btn btn-primary" type="submit">Pesquisar</button>
            </div>
        	    <a href="{{ route('notas.index') }}" class="btn btn-info pull-right mb-3">Voltar</a>
	        </div>
	    </form>
@endsection
